const { run } = require("hardhat");
const fs = require('fs')
const path = require('path');

// The environment variables are loaded in hardhat.config.ts
const mnemonic = process.env.MNEMONIC;
if (!mnemonic) {
    throw new Error("Please set your MNEMONIC in a .env file");
}

module.exports = {
    
    istanbulReporter: ["html", "lcov"],
    
    // Run TypeChain after compilation
    onCompileComplete: async function (_config) {
        try {
            await run("typechain");
        } catch (error) {
            console.error("Error running Typechain:", error);
            throw error;
        }
    },

    // Clean up artifacts generated by solcover
    onIstanbulComplete: async function (_config) {
        const artifactsPath = path.resolve(__dirname, "./artifacts");
        try {
            fs.promises.rm(artifactsPath, { recursive: true, force: true });
        } catch (error) {
            console.error("Error removing artifacts:", error);
            throw error;
        }
    },
    
    providerOptions: {
        mnemonic,
    },
    
    // Skip mocks and test files from coverage
    skipFiles: ["mocks", "test"],
};
