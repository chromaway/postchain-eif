package net.postchain.eif

import net.postchain.base.gtv.BlockHeaderData
import net.postchain.base.snapshot.SimpleDigestSystem
import net.postchain.common.BlockchainRid
import net.postchain.common.data.EMPTY_HASH
import net.postchain.common.data.HASH_LENGTH
import net.postchain.common.data.Hash
import net.postchain.common.data.KECCAK256
import net.postchain.common.hexStringToByteArray
import net.postchain.common.toHex
import net.postchain.concurrent.util.get
import net.postchain.core.Transaction
import net.postchain.crypto.KeyPair
import net.postchain.crypto.Secp256K1CryptoSystem
import net.postchain.crypto.devtools.KeyPairHelper
import net.postchain.devtools.IntegrationTestSetup
import net.postchain.devtools.PostchainTestNode
import net.postchain.devtools.testinfra.BaseTestInfrastructureFactory
import net.postchain.eif.MerkleProofUtil.getMerkleProof
import net.postchain.gtv.GtvByteArray
import net.postchain.gtv.GtvFactory.gtv
import net.postchain.gtv.GtvInteger
import net.postchain.gtv.GtvNull
import net.postchain.gtv.merkle.GtvMerkleHashCalculatorV2
import net.postchain.gtx.GtxBuilder
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigInteger
import java.security.MessageDigest

class EifBlockBuilderIT : IntegrationTestSetup() {

    val myCS = Secp256K1CryptoSystem()
    val merkleHashCalculator = GtvMerkleHashCalculatorV2(myCS)

    private lateinit var ds: SimpleDigestSystem
    private val sigMaker = myCS.buildSigMaker(KeyPair(KeyPairHelper.pubKey(0), KeyPairHelper.privKey(0)))
    fun makeEifEventOp(bcRid: BlockchainRid, num: Long): ByteArray {
        val b = GtxBuilder(bcRid, listOf(KeyPairHelper.pubKey(0)), myCS, merkleHashCalculator)
        b.addOperation(
                "eif_event",
                gtv(num),
                gtv(ds.digest(BigInteger.valueOf(num).toByteArray()))
        )
        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

    fun makeEifStateOp(bcRid: BlockchainRid, num: Long): ByteArray {
        val b = GtxBuilder(bcRid, listOf(KeyPairHelper.pubKey(0)), myCS, merkleHashCalculator)
        b.addOperation(
                "eif_state",
                gtv(num),
                gtv(num),
                gtv(ds.digest(BigInteger.valueOf(num).toByteArray()))
        )
        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

    fun makeNOPGTX(bcRid: BlockchainRid): ByteArray {
        val b = GtxBuilder(bcRid, listOf(KeyPairHelper.pubKey(0)), myCS, merkleHashCalculator)
        b.addOperation("nop", gtv(42))
        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

    fun makeTestTx(id: Long, value: String, bcRid: BlockchainRid): ByteArray {
        val b = GtxBuilder(bcRid, listOf(KeyPairHelper.pubKey(0)), myCS, merkleHashCalculator)
        b.addOperation("gtx_test", gtv(id), gtv(value))
        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

    fun makeTimeBTx(from: Long, to: Long?, bcRid: BlockchainRid): ByteArray {
        val b = GtxBuilder(bcRid, listOf(KeyPairHelper.pubKey(0)), myCS, merkleHashCalculator)
        b.addOperation(
                "timeb",
                gtv(from),
                if (to != null) gtv(to) else GtvNull
        )
        // Need to add a valid dummy operation to make the entire TX valid
        b.addOperation("gtx_test", gtv(1), gtv("true"))
        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

    @Test
    fun testEifBuildBlock() {
        configOverrides.setProperty("infrastructure", BaseTestInfrastructureFactory::class.qualifiedName)
        val nodes = createNodes(1, "/net/postchain/eif/blockchain_config.xml")
        val node = nodes[0]
        val bcRid = systemSetup.blockchainMap[1]!!.rid // Just assume we have chain 1
        ds = SimpleDigestSystem(MessageDigest.getInstance(KECCAK256))

        fun enqueueTx(data: ByteArray): Transaction? {
            try {
                val tx = node.getBlockchainInstance().blockchainEngine.getConfiguration().getTransactionFactory()
                        .decodeTransaction(data)
                node.getBlockchainInstance().blockchainEngine.getTransactionQueue().enqueue(tx)
                return tx
            } catch (e: Exception) {
                logger.error(e) { "Can't enqueue tx" }
            }
            return null
        }

        val validTxs = mutableListOf<Transaction>()
        var currentBlockHeight = -1L

        fun makeSureBlockIsBuiltCorrectly() {
            currentBlockHeight += 1
            buildBlockAndCommit(node.getBlockchainInstance().blockchainEngine)
            assertEquals(currentBlockHeight, getLastHeight(node))
            val ridsAtHeight = getTxRidsAtHeight(node, currentBlockHeight)
            for (vtx in validTxs) {
                val vtxRID = vtx.getRID()
                assertTrue(ridsAtHeight.any { it.contentEquals(vtxRID) })
            }
            validTxs.clear()
        }

        // Tx1 valid)
        val validTx1 = enqueueTx(makeTestTx(1, "true", bcRid))!!
        validTxs.add(validTx1)

        // Tx 2 invalid, b/c bad args
        enqueueTx(makeTestTx(2, "false", bcRid))!!

        // Tx 3: Nop (invalid, since need more ops)
        val x = makeNOPGTX(bcRid)
        enqueueTx(x)

        // Tx: EIF Event Op
        val validTx2 = enqueueTx(makeEifEventOp(bcRid, 1L))!!
        validTxs.add(validTx2)

        // Tx: EIF Account State Op
        val validTx3 = enqueueTx(makeEifStateOp(bcRid, 2L))!!
        validTxs.add(validTx3)

        // -------------------------
        // Build it
        // -------------------------
        makeSureBlockIsBuiltCorrectly()

        val blockHeaderData = getBlockHeaderData(node, currentBlockHeight)
        val extraData = blockHeaderData.gtvExtra
        val eifData = extraData[EIF]?.asByteArray()
        val eifRootEvent = eifData?.sliceArray(0 until HASH_LENGTH)
        val eifRootState = eifData?.sliceArray(HASH_LENGTH until 2 * HASH_LENGTH)
        val eventData =
                "00000000000000000000000000000000000000000000000000000000000000015fe7f977e71dba2ea1a68e21057beebb9be2ac30c6410aa38d4f3fbe41dcffd2".hexStringToByteArray()
        val eventHash = ds.hash(ds.hash(ds.digest(eventData), EMPTY_HASH), EMPTY_HASH)
        val stateData =
                "0000000000000000000000000000000000000000000000000000000000000002f2ee15ea639b73fa3db9b34a245bdfa015c260c598b211bf05a1ecc4b3e3b4f2".hexStringToByteArray()
        val stateHash = ds.hash(ds.hash(ds.digest(stateData), EMPTY_HASH), EMPTY_HASH)
        assertEquals(eventHash.toHex(), eifRootEvent!!.toHex())
        assertEquals(stateHash.toHex(), eifRootState!!.toHex())

        // Tx 4: time, valid, no stop is ok
        val tx4Time = makeTimeBTx(0, null, bcRid)
        validTxs.add(enqueueTx(tx4Time)!!)

        // Tx 5: time, valid, from beginning of time to now
        val tx5Time = makeTimeBTx(0, System.currentTimeMillis(), bcRid)
        validTxs.add(enqueueTx(tx5Time)!!)

        // TX 6: time, invalid since from bigger than to
        val tx6Time = makeTimeBTx(100, 0, bcRid)
        enqueueTx(tx6Time)

        // TX 7: time, invalid since from is in the future
        val tx7Time = makeTimeBTx(System.currentTimeMillis() + 100, null, bcRid)
        enqueueTx(tx7Time)

        // -------------------------
        // Build it
        // -------------------------
        makeSureBlockIsBuiltCorrectly()

        val value = node.getBlockchainInstance().blockchainEngine.getBlockQueries().query("gtx_test_get_value", gtv(mapOf(
                "txRID" to gtv(validTx1.getRID().toHex())
        )))
        assertEquals(gtv(listOf(gtv("true"))), value.get())
    }

    @Test
    fun testEifUpdateSnapshot() {
        configOverrides.setProperty("infrastructure", BaseTestInfrastructureFactory::class.qualifiedName)
        val nodes = createNodes(1, "/net/postchain/eif/blockchain_config_1.xml")
        val node = nodes[0]
        val bcRid = systemSetup.blockchainMap[1]!!.rid // Just assume we have chain 1
        ds = SimpleDigestSystem(MessageDigest.getInstance(KECCAK256))

        fun enqueueTx(data: ByteArray): Transaction? {
            try {
                val tx = node.getBlockchainInstance().blockchainEngine.getConfiguration().getTransactionFactory()
                        .decodeTransaction(data)
                node.getBlockchainInstance().blockchainEngine.getTransactionQueue().enqueue(tx)
                return tx
            } catch (e: Exception) {
                logger.error(e) { "Can't enqueue tx" }
            }
            return null
        }

        var currentBlockHeight = -1L

        fun sealBlock() {
            currentBlockHeight += 1
            buildBlockAndCommit(node.getBlockchainInstance().blockchainEngine)
            assertEquals(currentBlockHeight, getLastHeight(node))
        }

        // enqueue txs that emit accounts' state
        val leafHashes = mutableMapOf<Long, Hash>()
        for (i in 0..15) {
            enqueueTx(makeEifStateOp(bcRid, i.toLong()))
            val l = i.toLong()
            val state = SimpleGtvEncoder.encodeGtv(
                    gtv(
                            GtvInteger(l),
                            GtvByteArray(ds.digest(BigInteger.valueOf(l).toByteArray()))
                    )
            )
            leafHashes[l] = ds.digest(state)
        }

        // build 1st block and commit
        sealBlock()

        // calculate state root
        val l01 = ds.hash(leafHashes[0]!!, leafHashes[1]!!)
        val l23 = ds.hash(leafHashes[2]!!, leafHashes[3]!!)
        val hash00 = ds.hash(l01, l23)
        val l45 = ds.hash(leafHashes[4]!!, leafHashes[5]!!)
        val l67 = ds.hash(leafHashes[6]!!, leafHashes[7]!!)
        val hash01 = ds.hash(l45, l67)
        val l89 = ds.hash(leafHashes[8]!!, leafHashes[9]!!)
        val l1011 = ds.hash(leafHashes[10]!!, leafHashes[11]!!)
        val hash10 = ds.hash(l89, l1011)
        val l1213 = ds.hash(leafHashes[12]!!, leafHashes[13]!!)
        val l1415 = ds.hash(leafHashes[14]!!, leafHashes[15]!!)
        val hash11 = ds.hash(l1213, l1415)
        val leftHash = ds.hash(hash00, hash01)
        val rightHash = ds.hash(hash10, hash11)
        val root = ds.hash(leftHash, rightHash)

        // query state root from block header's extra data
        val blockHeaderData = getBlockHeaderData(node, currentBlockHeight)
        val extraData = blockHeaderData.gtvExtra
        val eifData = extraData[EIF]?.asByteArray()
        val eifStateRoot = eifData?.sliceArray(HASH_LENGTH until 2 * HASH_LENGTH)

        assertEquals(root.toHex(), eifStateRoot!!.toHex())

        // Verify account state merkle proof
        for (pos in 0..15) {
            val args = gtv(
                    "blockHeight" to gtv(currentBlockHeight),
                    "accountNumber" to gtv(pos.toLong())
            )
            val gtvProof = node.getBlockchainInstance().blockchainEngine.getBlockQueries().query(
                    "get_account_state_merkle_proof",
                    args
            ).get().asDict()

            val merkleProofs = gtvProof["stateProof"]!!.asDict()["merkleProofs"]!!.asArray()
            val proofs = merkleProofs.map { it.asByteArray() }
            val stateRoot = getMerkleProof(proofs, pos, leafHashes[pos.toLong()]!!, ds::hash)
            assertEquals(stateRoot.toHex(), eifStateRoot.toHex())

            val pubkey = gtvProof["blockWitness"]!!.asArray()[0].asDict()["pubkey"]!!.asByteArray()
            assertEquals(pubkey.toHex(), getEthereumAddress(node.pubKey.hexStringToByteArray()).toHex())
        }

        val l = 16L
        enqueueTx(makeEifStateOp(bcRid, l))
        val state = SimpleGtvEncoder.encodeGtv(
                gtv(
                        GtvInteger(l),
                        GtvByteArray(ds.digest(BigInteger.valueOf(l).toByteArray()))
                )
        )
        leafHashes[l] = ds.digest(state)

        // build 2nd block and commit
        sealBlock()

        // query state root from block header's extra data
        val blockHeaderData2 = getBlockHeaderData(node, currentBlockHeight)
        val extraData2 = blockHeaderData2.gtvExtra
        val eifData2 = extraData2[EIF]?.asByteArray()
        val eifStateRoot2 = eifData2?.sliceArray(HASH_LENGTH until 2 * HASH_LENGTH)

        // calculate the new state root
        val p5 = ds.hash(ds.hash(leafHashes[l]!!, EMPTY_HASH), EMPTY_HASH)
        val p7 = ds.hash(ds.hash(p5, EMPTY_HASH), EMPTY_HASH)
        val root2 = ds.hash(ds.hash(root, p7), EMPTY_HASH)
        assertEquals(root2.toHex(), eifStateRoot2!!.toHex())

        for (pos in 0..16) {
            val args = gtv(
                    "blockHeight" to gtv(currentBlockHeight),
                    "accountNumber" to gtv(pos.toLong())
            )
            val gtvProof = node.getBlockchainInstance().blockchainEngine.getBlockQueries().query(
                    "get_account_state_merkle_proof",
                    args
            ).get().asDict()

            val merkleProofs = gtvProof["stateProof"]!!.asDict()["merkleProofs"]!!.asArray()
            val proofs = merkleProofs.map { it.asByteArray() }
            val stateRoot = getMerkleProof(proofs, pos, leafHashes[pos.toLong()]!!, ds::hash)
            assertEquals(stateRoot.toHex(), eifStateRoot2.toHex())

            val pubkey = gtvProof["blockWitness"]!!.asArray()[0].asDict()["pubkey"]!!.asByteArray()
            assertEquals(pubkey.toHex(), getEthereumAddress(node.pubKey.hexStringToByteArray()).toHex())
        }
    }

    @Test
    fun testEifEventAndUpdateSnapshot() {
        configOverrides.setProperty("infrastructure", BaseTestInfrastructureFactory::class.qualifiedName)
        val nodes = createNodes(1, "/net/postchain/eif/blockchain_config_1.xml")
        val node = nodes[0]
        val bcRid = systemSetup.blockchainMap[1]!!.rid // Just assume we have chain 1
        ds = SimpleDigestSystem(MessageDigest.getInstance(KECCAK256))

        fun enqueueTx(data: ByteArray): Transaction? {
            try {
                val tx = node.getBlockchainInstance().blockchainEngine.getConfiguration().getTransactionFactory()
                        .decodeTransaction(data)
                node.getBlockchainInstance().blockchainEngine.getTransactionQueue().enqueue(tx)
                return tx
            } catch (e: Exception) {
                logger.error(e) { "Can't enqueue tx" }
            }
            return null
        }

        var currentBlockHeight = -1L

        fun sealBlock() {
            currentBlockHeight += 1
            buildBlockAndCommit(node.getBlockchainInstance().blockchainEngine)
            assertEquals(currentBlockHeight, getLastHeight(node))
        }

        // enqueue txs that emit event
        val leafs = mutableListOf<Hash>()
        for (i in 1..4) {
            val l = i.toLong()
            enqueueTx(makeEifEventOp(bcRid, l))
            val event = SimpleGtvEncoder.encodeGtv(
                    gtv(
                            GtvInteger(l),
                            GtvByteArray(ds.digest(BigInteger.valueOf(l).toByteArray()))
                    )
            )
            leafs.add(ds.digest(event))
        }

        // enqueue txs that emit accounts' state
        val leafHashes = mutableMapOf<Long, Hash>()
        for (i in 0..15) {
            val l = i.toLong()
            enqueueTx(makeEifStateOp(bcRid, l))
            val state = SimpleGtvEncoder.encodeGtv(
                    gtv(
                            GtvInteger(l),
                            GtvByteArray(ds.digest(BigInteger.valueOf(l).toByteArray()))
                    )
            )
            leafHashes[l] = ds.digest(state)
        }

        // build 1st block and commit
        sealBlock()

        // calculate event root hash
        val l12 = ds.hash(leafs[0], leafs[1])
        val l34 = ds.hash(leafs[2], leafs[3])
        val eventRootHash = ds.hash(l12, l34)

        // calculate state root hash
        val l01 = ds.hash(leafHashes[0]!!, leafHashes[1]!!)
        val l23 = ds.hash(leafHashes[2]!!, leafHashes[3]!!)
        val hash00 = ds.hash(l01, l23)
        val l45 = ds.hash(leafHashes[4]!!, leafHashes[5]!!)
        val l67 = ds.hash(leafHashes[6]!!, leafHashes[7]!!)
        val hash01 = ds.hash(l45, l67)
        val l89 = ds.hash(leafHashes[8]!!, leafHashes[9]!!)
        val l1011 = ds.hash(leafHashes[10]!!, leafHashes[11]!!)
        val hash10 = ds.hash(l89, l1011)
        val l1213 = ds.hash(leafHashes[12]!!, leafHashes[13]!!)
        val l1415 = ds.hash(leafHashes[14]!!, leafHashes[15]!!)
        val hash11 = ds.hash(l1213, l1415)
        val leftHash = ds.hash(hash00, hash01)
        val rightHash = ds.hash(hash10, hash11)
        val stateRootHash = ds.hash(leftHash, rightHash)

        // query state root from block header's extra data
        val blockHeaderData = getBlockHeaderData(node, currentBlockHeight)
        val extraData = blockHeaderData.gtvExtra
        val eifData = extraData[EIF]?.asByteArray()
        val eifRootEvent = eifData?.sliceArray(0 until HASH_LENGTH)
        val eifRootState = eifData?.sliceArray(HASH_LENGTH until 2 * HASH_LENGTH)

        assertEquals(stateRootHash.toHex(), eifRootState!!.toHex())
        assertEquals(eventRootHash.toHex(), eifRootEvent!!.toHex())

        // Verify event merkle proof
        for (pos in 0..3) {
            val args = gtv(
                    "blockHeight" to gtv(currentBlockHeight),
                    "eventHash" to gtv(leafs[pos].toHex())
            )
            val gtvProof = node.getBlockchainInstance().blockchainEngine.getBlockQueries().query(
                    "get_event_merkle_proof",
                    args
            ).get().asDict()

            val merkleProofs = gtvProof["eventProof"]!!.asDict()["merkleProofs"]!!.asArray()
            val proofs = merkleProofs.map { it.asByteArray() }
            val eventRoot = getMerkleProof(proofs, pos, leafs[pos], ds::hash)
            assertEquals(eventRoot.toHex(), eventRootHash.toHex())

            val eventData = gtvProof["eventData"]!!.asByteArray()
            assertTrue(ds.digest(eventData).contentEquals(leafs[pos]))
            val eventHash = gtvProof["eventProof"]!!.asDict()["leaf"]!!.asByteArray()
            assertTrue(eventHash.contentEquals(leafs[pos]))

            val pubkey = gtvProof["blockWitness"]!!.asArray()[0].asDict()["pubkey"]!!.asByteArray()
            assertEquals(pubkey.toHex(), getEthereumAddress(node.pubKey.hexStringToByteArray()).toHex())
        }

        val l = 16L
        enqueueTx(makeEifStateOp(bcRid, l))
        val state = SimpleGtvEncoder.encodeGtv(
                gtv(
                        GtvInteger(l),
                        GtvByteArray(ds.digest(BigInteger.valueOf(l).toByteArray()))
                )
        )
        leafHashes[l] = ds.digest(state)

        // build 2nd block and commit
        sealBlock()

        // query state root from block header's extra data
        val blockHeaderData2 = getBlockHeaderData(node, currentBlockHeight)
        val extraData2 = blockHeaderData2.gtvExtra
        val eifData2 = extraData2[EIF]?.asByteArray()
        val eifStateRoot2 = eifData2?.sliceArray(HASH_LENGTH until 2 * HASH_LENGTH)

        // calculate state root in new block
        val p5 = ds.hash(ds.hash(leafHashes[l]!!, EMPTY_HASH), EMPTY_HASH)
        val p7 = ds.hash(ds.hash(p5, EMPTY_HASH), EMPTY_HASH)
        val root2 = ds.hash(ds.hash(stateRootHash, p7), EMPTY_HASH)
        assertEquals(root2.toHex(), eifStateRoot2!!.toHex())
    }

    private fun getBlockHeaderData(node: PostchainTestNode, height: Long): BlockHeaderData {
        val blockQueries = node.getBlockchainInstance().blockchainEngine.getBlockQueries()
        val blockHeader = blockQueries.getBlockAtHeight(height).get()!!.header
        return BlockHeaderData.fromBinary(blockHeader.rawData)
    }
}