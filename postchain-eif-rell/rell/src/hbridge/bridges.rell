entity bridge_contract {
    network_id: integer;
    contract_address: byte_array;
    key network_id, contract_address;
}

entity bridge_erc20_asset {
    key bridge_contract, erc20_asset;
}

function register_bridge_and_erc20_asset(
    network_id: integer,
    token_address: byte_array,
    ft4.assets.asset,
    mode: bridge_mode,
    use_snapshots: boolean = false,
    bridge_address: byte_array
) {
    val erc20_asset = register_erc20_asset(network_id, token_address, asset, mode, use_snapshots);
    val bridge_contract = get_or_create_bridge(network_id, bridge_address);
    create bridge_erc20_asset(bridge_contract, erc20_asset);
}

function register_bridge_with_erc20_assets(network_id: integer, bridge_address: byte_array, erc20_assets: list<erc20_asset>) {
    val bridge_contract = get_or_create_bridge(network_id, bridge_address);

    for (erc20_asset in erc20_assets) {
        require(erc20_asset.network_id == bridge_contract.network_id, "Bridge must be part of the same network as the asset");
        create bridge_erc20_asset(bridge_contract, erc20_asset);
    }
}

function get_or_create_bridge(network_id: integer, bridge_address: byte_array) {
    return bridge_contract @? { network_id, bridge_address }
        ?: create bridge_contract(network_id, bridge_address);
}
