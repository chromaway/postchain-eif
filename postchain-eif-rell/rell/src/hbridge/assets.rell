
// link between ERC20 token and FT4 asset
entity erc20_asset {
    network_id: integer;
    token_address: byte_array; // token contract address
    key network_id, token_address;
    bridge_mode;
    index ft4.assets.asset;
    key virtual_blockchain_rid: byte_array; // virtual blockchain RID derived from network_id and address
}

struct erc20_info {
    network_id: integer;
    token_address: byte_array; // contract address
    bridge_mode;
    virtual_blockchain_rid: byte_array; // virtual blockchain RID derived from network_id and address
}

struct erc20_asset_info {
    network_id: integer;
    token_address: byte_array; // contract address
    bridge_mode;
    ft_asset_id: byte_array;
    virtual_blockchain_rid: byte_array; // virtual blockchain RID derived from network_id and address
}

struct bridge_contract_info {
    contract_address: byte_array;
    evm_read_offset: integer;
    erc20_assets: list<erc20_asset_info>;
}

function register_erc20_asset(
    network_id: integer,
    token_address: byte_array,
    ft4.assets.asset,
    mode: bridge_mode,
    use_snapshots: boolean = false
): erc20_asset {
    require(empty(erc20_asset @? { network_id, token_address }), "ERC-20 Token already exists: %s".format(token_address));
    val erc20_asset = create erc20_asset (
        network_id,
        token_address = token_address,
        mode,
        asset,
        virtual_blockchain_rid = compute_virtual_blockchain_rid(network_id, token_address)
    );
    if (use_snapshots) {
        enable_erc20_asset_snapshots(erc20_asset);
    }
    return erc20_asset;
}

function compute_virtual_blockchain_rid(network_id: integer, token_address: byte_array): byte_array =
        ("EVM", network_id, token_address).hash();

function map_erc20_asset_to_erc20_asset_info(erc20_asset) = erc20_asset_info(
    network_id = erc20_asset.network_id,
    token_address = erc20_asset.token_address,
    bridge_mode = erc20_asset.bridge_mode,
    ft_asset_id = erc20_asset.asset.id,
    virtual_blockchain_rid = erc20_asset.virtual_blockchain_rid
);
