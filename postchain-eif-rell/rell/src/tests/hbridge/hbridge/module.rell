@test module;

import lib.icmf.test.{ test_icmf_message };
import lib.ft4.auth;
import lib.ft4.admin;
import lib.ft4.accounts;
import lib.ft4.assets;
import ft4_utils: lib.ft4.utils;
import lib.ft4.accounts.strategies;
import create_on_transfer: lib.ft4.accounts.strategies.transfer;
import lib.ft4.accounts.strategies.transfer.open;
import lib.ft4.crosschain;
import admin_crosschain: lib.ft4.external.admin.crosschain;
import ft4_test: lib.ft4.test.core;

// event_receiver
import eif.utils;
import eif_event_receiver.*;
import eif_event_receiver.test.helpers.*;
import eif_event_connector.*;

import eif_utils: eif.utils;
import eif.common;
import eif.messaging;
import hbridge;

import ^.helpers;

val admin_privkey = x"3132333435363738393031323334353637383930313233343536373839303131";

namespace evm_events {
    val link_account_id_name = "LinkAccountID";
    val deposited_erc20_name = "DepositedERC20";
    val pending_withdraw_event = "PendingWithdraw";
    val unpending_withdraw_event = "UnpendingWithdraw";
    val withdraw_request_event = "WithdrawRequestHash";
    val withdrawal_hash_event = "WithdrawalHash";
    val withdrawal_to_postchain_event = "WithdrawalToPostchain";
}

val contract_address = x"b7eB5d60Dd5610A82A4844Beb3F7d750316d3D32";
val network_id1 = 1;
val network_id2 = 2;
val bridge_address1 = x"b7eB5d60Dd5610A82A4844Beb3F7d75030000001";
val bridge_address2 = x"b7eB5d60Dd5610A82A4844Beb3F7d75030000002";
val bridge_address3 = x"b7eB5d60Dd5610A82A4844Beb3F7d75030000003";

struct asset_info {
    token_address: byte_array;
    token_name: text;
    token_symbol: text;
    token_decimal: integer;
    token_icon_url: text;
}

val CHR = asset_info(
    token_address = x"39615b16b74589919c9ce1ea73f1fc5d53141a78",
    token_name = "Chromia",
    token_symbol = "CHR",
    token_decimal = 6,
    token_icon_url = "https://chromaway.com/chr",
);

val FOO = asset_info(
    token_address = x"39615b16b74589919c9ce1ea73f1fc5d53141a79",
    token_name = "FOO Token",
    token_symbol = "FOO",
    token_decimal = 6,
    token_icon_url = "https://foo.com/foo",
);

val BAR = asset_info(
    token_address = x"39615b16b74589919c9ce1ea73f1fc5d53141a70",
    token_name = "BAR Token",
    token_symbol = "BAR",
    token_decimal = 6,
    token_icon_url = "https://bar.com/bar",
);

val ABC = asset_info(
    token_address = x"39615b16b74589919c9ce1ea73f1fc5d53141a71",
    token_name = "ABC Token",
    token_symbol = "ABC",
    token_decimal = 6,
    token_icon_url = "https://abc.com/abc",
);

function register_foreign_asset(asset: asset_info = CHR, network_id: integer = network_id1): assets.asset {
    val ft4_asset = register_ft4_asset(asset);
    register_erc20_asset(ft4_asset, asset, network_id, enable_snapshots = true, bridge_mode = hbridge.bridge_mode.foreign);
    return ft4_asset;
}

function register_native_asset(asset: asset_info = CHR, network_id: integer = network_id1): erc20_asset {
    val ft4_asset = register_ft4_asset(asset);
    return register_erc20_asset(ft4_asset, asset, network_id, enable_snapshots = true, bridge_mode = hbridge.bridge_mode.native);
}

function register_ft4_asset(asset_info: asset_info): assets.asset {
    val asset_id = (asset_info.token_name, chain_context.blockchain_rid).hash();

    rell.test.tx().op(
        admin.external.register_asset(asset_info.token_name, asset_info.token_symbol, asset_info.token_decimal, asset_info.token_icon_url)
    ).sign(admin_privkey).run();

    return assets.Asset(asset_id);
}

function register_erc20_asset(
    ft4_asset: assets.asset,
    asset_info: asset_info,
    network_id: integer,
    enable_snapshots: boolean = false,
    hbridge.bridge_mode = hbridge.bridge_mode.foreign
): erc20_asset {
    rell.test.tx().op(
        helpers.register_erc20_asset_op(network_id, asset_info.token_address, ft4_asset, bridge_mode, enable_snapshots)
    ).run();

    assert_equals(
        hbridge.get_erc20_for_asset(ft4_asset.id) @? { .network_id == network_id, .token_address == asset_info.token_address },
        hbridge.erc20_info(
            network_id,
            token_address = asset_info.token_address,
            bridge_mode = bridge_mode,
            virtual_blockchain_rid = hbridge.compute_virtual_blockchain_rid(network_id, asset_info.token_address)
        )
    );

    return erc20_asset @ { network_id, .token_address == asset_info.token_address };
}

function register_crosschain_asset(asset_info, issuing_blockchain_rid: byte_array, origin_blockchain_rid: byte_array): ft4.crosschain.asset_origin {
    val asset_id = (asset_info.token_name, issuing_blockchain_rid).hash();

    rell.test.tx().op(
        admin_crosschain.register_crosschain_asset(
            id = asset_id,
            name = asset_info.token_name,
            symbol = asset_info.token_symbol,
            decimals = asset_info.token_decimal,
            issuing_blockchain_rid = issuing_blockchain_rid,
            icon_url = asset_info.token_icon_url,
            type = assets.ASSET_TYPE_FT4,
            uniqueness_resolver = x"",
            origin_blockchain_rid = origin_blockchain_rid
        )
    ).sign(admin_privkey).run();

    return ft4.crosschain.asset_origin @ { .asset.id == asset_id };
}

function register_bridge_and_foreign_asset(asset: asset_info, network_id: integer, bridge_address: byte_array): assets.asset {
    val ft4_asset = register_ft4_asset(asset);
    val erc20_asset = register_erc20_asset(ft4_asset, asset, network_id, enable_snapshots = true, bridge_mode = hbridge.bridge_mode.foreign);
    // register bridge with erc20 asset
    rell.test.tx().op(
        helpers.register_bridge_with_erc20_assets_op(
            network_id = network_id,
            bridge_contract_address = bridge_address,
            erc20_assets = [erc20_asset]
        )
    ).run();

    return ft4_asset;
}

function register_bridge_with_erc20_assets(network_id: integer, bridge_address: byte_array, erc20_assets: list<hbridge.erc20_asset>) {
    rell.test.tx().op(
        helpers.register_bridge_with_erc20_assets_op(
            network_id = network_id,
            bridge_contract_address = bridge_address,
            erc20_assets = erc20_assets
        )
    ).run();
}

function link_eoa_account(
        evm_keypair: rell.test.keypair,
        ft_keypair: rell.test.keypair,
        account_id: byte_array,
        auth_descriptor_id: byte_array
) {
    val evm_address = crypto.eth_pubkey_to_address(evm_keypair.pub);
    val op = hbridge.link_evm_eoa_account(evm_address);
    val message = ft4_test.create_evm_auth_message(op)
        .replace(auth.ACCOUNT_ID_PLACEHOLDER, account_id.to_hex().upper_case())
        .replace(auth.AUTH_DESCRIPTOR_ID_PLACEHOLDER, auth_descriptor_id.to_hex().upper_case());
    val signature = ft4_test.evm_sign(message, evm_keypair.priv);
    rell.test.tx()
        .op(auth.evm_signatures([evm_address], [signature]))
        .op(auth.external.ft_auth(account_id, auth_descriptor_id))
        .op(op)
        .nop()
        .sign(ft_keypair).run();
    assert_not_null(hbridge.evm_eoa_account_link @? { .address == evm_address });

    assert_equals(hbridge.get_account_for_eoa_address(evm_address), account_id);
    assert_equals(hbridge.get_eoa_addresses_for_account(account_id), [evm_address]);
}

function link_sc_account_id_event(
    evm_address: byte_array,
    account_id: byte_array,
    block_height: integer = 1,
    network_id: integer = network_id1
) {
    val events = [
        event_data(
            tnx_hash = ("TX", block_height).hash(),
            log_index = 1,
            signature = ("SIG", block_height).hash(),
            contract_address = contract_address,
            name = evm_events.link_account_id_name,
            indexed_values = [evm_address.to_gtv()],
            non_indexed_values = [account_id.to_gtv(), true.to_gtv()]
        )
    ];
    val block_hash = ("BLOCK", block_height).hash();
    rell.test.tx().nop().op(
        evm_block_op(network_id, block_height, block_hash, events)
    ).run();
}

function link_sc_account(
        evm_keypair: rell.test.keypair,
        ft_keypair: rell.test.keypair,
        account_id: byte_array,
        auth_descriptor_id: byte_array,
        network_id: integer = network_id1
) {
    val evm_address = crypto.eth_pubkey_to_address(evm_keypair.pub);
    val op = hbridge.link_evm_sc_account(evm_address, network_id);
    val message = ft4_test.create_evm_auth_message(op)
        .replace(auth.ACCOUNT_ID_PLACEHOLDER, account_id.to_hex().upper_case())
        .replace(auth.AUTH_DESCRIPTOR_ID_PLACEHOLDER, auth_descriptor_id.to_hex().upper_case());
    val signature = ft4_test.evm_sign(message, evm_keypair.priv);
    rell.test.tx()
        .op(auth.external.ft_auth(account_id, auth_descriptor_id))
        .op(op)
        .nop()
        .sign(ft_keypair).run();
    assert_not_null(hbridge.evm_sc_account_link @? { .address == evm_address });

    assert_equals(hbridge.get_account_for_sc_address(evm_address, network_id), account_id);
    assert_equals(hbridge.get_sc_addresses_for_account(account_id), [(address = evm_address, network_id = network_id)]);
}

function deposit_from_evm(
    sender_address: byte_array,
    amount: big_integer,
    block_height: integer = 1,
    account_id: byte_array = hbridge.zero_beneficiary,
    asset: asset_info = CHR,
    network_id: integer = network_id1,
    contract_address: byte_array = contract_address
) {
    val events = [
        event_data(
            tnx_hash = ("TX", block_height).hash(),
            log_index = 1,
            signature = ("SIG", block_height).hash(),
            contract_address = contract_address,
            name = evm_events.deposited_erc20_name,
            indexed_values = [sender_address.to_gtv(), asset.token_address.to_gtv()],
            non_indexed_values = [amount.to_gtv(), account_id.to_gtv()]
        )
    ];
    val block_hash = ("BLOCK", block_height).hash();
    rell.test.tx().nop().op(
        evm_block_op(network_id, block_height, block_hash, events)
    ).run();
}

function trigger_withdrawal_status_on_evm(
    withdrawal_event_hash: byte_array,
    event_name: text,
    block_height: integer = 1,
    network_id: integer = network_id1,
) {
    val events = [
        event_data(
            tnx_hash = ("TX", block_height).hash(),
            log_index = 1,
            signature = ("SIG", block_height).hash(),
            contract_address = contract_address,
            name = event_name,
            indexed_values = [withdrawal_event_hash.to_gtv()],
            non_indexed_values = []
        )
    ];
    val block_hash = ("BLOCK", block_height).hash();
    rell.test.tx().nop().op(
        evm_block_op(network_id, block_height, block_hash, events)
    ).run();
}

function run_block_after_timeout() {
    rell.test.set_next_block_time(rell.test.last_block_time + (ft4_utils.MILLISECONDS_PER_DAY * 10) + 1);
    rell.test.block().run();
}

function assert_withdraw(
    serial: integer,
    evm_address: byte_array,
    amount: big_integer,
    asset: asset_info = CHR,
    expected_nr_withdrawals: integer = 1,
    network_id: integer = network_id1,
    bridge_address: byte_array = x"",
    version: integer = 1
) {
    val timestamp = rell.test.last_block_time_or_null ?: 0;
    val events = rell.test.get_events();

    val event_data = if (events.size() > 1)
        events @* { $[0] == "eif_event" } [0]
    else
        events[0];

    val withdraw_info = if (version == 2)
        pack_withdrawal_event_v2(serial, network_id, asset.token_address, evm_address, bridge_address, amount)
    else
        pack_withdrawal_event_v1(serial, network_id, asset.token_address, evm_address, amount);

    assert_equals(event_data, ("eif_event", withdraw_info[0]));

    val wdrs = hbridge.get_erc20_withdrawal(network_id, asset.token_address, evm_address);
    assert_equals(wdrs.size(), expected_nr_withdrawals);
    val wdr = wdrs @ {} (@omit @sort_desc .serial, $) limit 1;
    assert_equals(
        (wdr.serial, wdr.network_id, wdr.token_address, wdr.beneficiary, wdr.amount, wdr.transaction.block.timestamp),
        (serial, network_id, asset.token_address, evm_address, amount, timestamp)
    );
}

function create_ft4_account(pubkey: byte_array) {
    val auth_descriptor = ft4_test.create_auth_descriptor(pubkey, ["A", "T"]);

    rell.test.tx().op(
        admin.external.register_account(auth_descriptor)
    ).sign(admin_privkey).run();

    val account_id = pubkey.hash();
    val ft_account = accounts.Account(account_id);

    return (
        ft_account, auth_descriptor
    );
}

function _mint(accounts.account, assets.asset, amount: big_integer = 10000000000000000000L): big_integer {
    rell.test.tx().op(
        admin.external.mint(account.id, asset.id, amount)
    ).sign(admin_privkey).run();
    return amount;
}

struct account_data {
    accounts.account;
    auth_descriptor_id: byte_array;
    rell.test.keypair;
    evm_address: byte_array;
    ft_auth: () -> rell.test.op;
}

function create_and_link_account(ft_keypair: rell.test.keypair, evm_keypair: rell.test.keypair): account_data {
    // Auth descriptor
    val account_id = ft_keypair.pub.hash();
    val auth_descriptor = ft4_test.create_auth_descriptor(ft_keypair.pub, ["A", "T"]);
    val auth_descriptor_id = auth_descriptor.hash();

    // Register account
    rell.test.tx().op(
        admin.external.register_account(auth_descriptor)
    ).sign(admin_privkey).run();
    val ft_account = accounts.Account(account_id);

    // Link account
    link_eoa_account(
        evm_keypair,
        ft_keypair,
        account_id,
        auth_descriptor_id
    );

    return account_data(
        account = accounts.account @ { .id == account_id },
        auth_descriptor_id = auth_descriptor_id,
        keypair = ft_keypair,
        evm_address = crypto.eth_pubkey_to_address(evm_keypair.pub),
        ft_auth = auth.external.ft_auth(account_id, auth_descriptor_id, *),
    );
}

function get_account_id_and_auth_descriptor_id(ft_keypair: rell.test.keypair): (byte_array, byte_array) {
    val account_id = ft_keypair.pub.hash();
    val auth_descriptor = ft4_test.create_auth_descriptor(ft_keypair.pub, ["A", "T"]);
    val auth_descriptor_id = auth_descriptor.hash();
    return (account_id, auth_descriptor_id);
}
